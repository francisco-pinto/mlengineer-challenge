Introduction
============

This document will guide you through the tasks we expect you to complete.

Summary
=======

The goal of this challenge is to perform a statistical ML-based analysis of a graph, representing paper co-authorships at EPFL. In particular, we wish to understand how the *past & present structure of the graph* can predict its *future structure*.

Consider the example illustrated in Figure 1. The graph in Figure 1(a) shows all paper co-authorships from 2014 to 2017, where each node represents a person and each edge represents the number of papers co-authored together. In Figure 1(b), the same graph is illustrated with additional edges in red colour, representing all new (first time) co-authorships in the year 2018.

![Graph](https://bitbucket.org/francisco-pinto/mlengineer-challenge/downloads/Graph2.png)

**Figure 1.** Predicting future research collaborations based on past paper co-authorships. **(a)** The graph on the left shows paper co-authorships from 2014 to 2017. **(b)** The graph on the right shows paper co-authorships from 2014 to 2017 (black colour), plus co-authorships in 2018 (red colour). In both figures, nodes represent people and edges represent number of papers co-authored.

In this example, the question we wish to answer is: *How accurately can we predict the 2018 new co-authorships based on the historical co-authorships of 2014 up to 2017?*

To answer this question, your task is to train a statistical model using conventional machine learning techniques, which will be capable of predicting a subset of the co-authorships dataset based on another subset.

Specifications
==============

For this exercise, you are given a dataset consisting of 3 json files:

- [Person_Lab_Map.json](https://bitbucket.org/francisco-pinto/mlengineer-challenge/downloads/Person_Lab_Map.json) - list of people and their labs;
- [Publication_Person_Map.json](https://bitbucket.org/francisco-pinto/mlengineer-challenge/downloads/Publication_Person_Map.json) - list of publications and their authors;
- [Publication_Year.json](https://bitbucket.org/francisco-pinto/mlengineer-challenge/downloads/Publication_Year.json) - year of publications.

With this dataset, you can re-create the graph of historical co-authorships at EPFL from 2003 to 2021. Your solution begins by defining 3 parameters: the (simulated) present year, the year range towards the past, and the year range towards the future. See the example in Figure 2.

![Timeline](https://bitbucket.org/francisco-pinto/mlengineer-challenge/downloads/Timeline.png)

**Figure 2.** Sample publications timeline. In this example, the present year is 2011 (black). The goal is to use data from 2008 up to the present year (blue + black) to predict co-authorships from 2012 to 2013 (red). In your solution, the present year, range towards the past, and range towards the future should be defined as input parameters.

The next step is to train a statistical model by splitting the dataset into the traditional *training* + *evaluation* subsets. This model should be capable of predicting co-authorship edges in the *future years* based on data from the *past & present years*.

You may use any statistical model (or combination thereof) of your choice. The quality of your classifier should be measured in terms of accuracy, precision, recall, f-score, and AUC.

The input feature set can be constructed as you see fit.

Your solution must be programmed in Python using a Jupyter Notebook, which should contain both your code and a complete description of your solution (using Jupyter's markdown cells). Please note that, even if you don't achieve high predictive scores, it is still important for us to understand your reasoning. So, make sure you clearly explain all your steps.

We will discuss your solution in the next interview.

Technologies
============

To develop your co-authorships predictive model, you may use any Python libraries of your choice. Here's a few examples (not limited to):

- **Data manipulation:** numpy, scipy, pandas, networkx
- **Statistical modelling:** statsmodels, sklearn;
- **Visualisation:** matplotlib, ipycytoscape.

Assessment
==========

We are looking for someone not only with experience but also self-motivated and able to develop creative solutions. We welcome thinking out of the box! This assessment is designed to challenge you, and provide an instructive and fun experience.

Extra kudos if your solution is well documented. We like people who are able to learn and do their research. Even if you might not get the tasks completed, your notes will help us in the evaluation of your work.

Guidelines
==========

- Please try and complete the assessment **within 1 week** of receiving it;
- Create repository to track and share your work. Please link commits relevant to each task completed. We recommend you create a Bitbucket account and a private git repository (there's no cost to you). Invite "francisco-pinto" to your repository team;
- Share your results even if you don't finish;
- We understand there's more than one way to do things. If you're stuck, pick one and justify your choice.


Assumptions
===========

You are proficient in Python, comfortable with graphs and machine learning packages, and capable of quickly testing a statistical classifier on well organised data.


Rules
=====

Please keep this assessment to yourself, i.e. don't share it with you colleagues, friends and the rest of the world.
Keep the repository private.

You, and you alone, are expected to be the unique author of the solutions for this assessment. You will be questioned on details of the solution you provide.


Feedback
========

Please feel free to provide any feedback you have about this the assessment or even propose any technical and not technical improvement.